# PostgreSQL HA Cluster with Pacemaker and DRBD

### Links
* https://clck.ru/32dR8B - DRBD Theory
* https://clck.ru/32dRHN - Pacemaker Theory
* https://clck.ru/32dQku - CENTOS7 DRBD9 LVM
* https://clck.ru/32dQpK - УСТАНОВКА И НАСТРОЙКА PACEMAKER НА CENTOS7
* https://clck.ru/32dQoH - ПОСТРОЕНИЕ ОТКАЗОУСТОЙЧИВОГО КЛАСТЕРА DRBD9 И ПАКЕТА PACEMAKER НА CENTOS7
* https://clck.ru/32dQsc - Monitoring of Pacemaker based HA clusters
* https://clck.ru/32dQvi - Grafana dashboards

### Ansible roles
* https://clck.ru/32dR4Z - Ansible DRBD Role
* https://clck.ru/32dR6b - Ansible Pacemaker Role